# My cryptography skills ARE ASS AND SHIT and I'm sure this can be improved
# DON'T RELY ON MY PROGRAM FOR ANY REAL SECURITY
# YOU HAVE BEEN WARNED. 
# I'd like to think it's secure though but I'm not sure.
# Output is going to be base64(SHA512(master password + ":" + domain))

import hashlib #sha512
from base64 import b64encode

# sha512 hash it and then base64 encode it
def base64_sha512(password):
    bpassword = bytes(password, "utf8")
    sha512 = hashlib.sha512(bpassword)
    base64 = b64encode(bytes.fromhex(sha512.hexdigest())).decode() # from hex to binary and then decode to string
    return base64

def gen(password,domain,length=None):
    # defaut length is 12
    if length is None:
        length = 12
    else:
        length = int(length)

    genpass = password + ":" + domain

    # rounds of hashing and encoding
    # doing a shit ton of rounds, may be slow on older computers though so might work on allowing
    #user to choose amount of rounds or choosing a sensible default
    for i in range(1000000):
        genpass = base64_sha512(genpass)
    
    return genpass[:length]
