import sys, getopt
import tkinter as tk
from pgen import *

argv = sys.argv[1:]
password = ""
domain = ""
length = ""

# CLI or GUI
if len(argv)==6 or not len(argv)==0:
    try:
        # Check if all arguments completed
        if not len(argv) == 6:
            raise Exception
        opts, args = getopt.getopt(argv,"p:d:l:",
                ["password =","domain =","length ="])
        for opt, arg in opts:
            if opt in ["-p","--password"]:
                password = arg
            elif opt in ["-d","--domain"]:
                domain = arg
            elif opt in ["-l","--length"]:
                length = arg
        print("Generated Password: "+ str(gen(password,domain,length)))
    except:
        print("Error:\nFormat is python main.py -p password -d domain -l length")
else:
    root = tk.Tk()
    root.title("Password Generator")
    root.geometry('750x300')
    
    titlelbl = tk.Label(root,text="Password Generator", font=("Arial",21)).grid(column=1,row=0)
    # Hacky way just to make the UI look better. TK doesn't display empty rows :(
    defaultcolor = root.cget("bg")
    spacer = tk.Label(root,text="",bg=defaultcolor)
    spacer.grid(column=0,row=1)
    # Create pass/domain/length labels and entry field
    passlbl = tk.Label(root, text="Password:", font=("Arial", 21)).grid(column=0, row=2)
    passent = tk.Entry(root,width=30)
    passent.grid(column=1, row=2)
    passent.focus()
    domainlbl = tk.Label(root, text="Domain:", font=("Arial",21)).grid(column=0,row=3)
    domainent = tk.Entry(root,width=30)
    domainent.grid(column=1, row=3)
    lenlbl = tk.Label(root, text="Length:", font=("Arial",21)).grid(column=0,row=4)
    lenent = tk.Entry(root,width=30)
    lenent.grid(column=1,row=4)
    lenent.insert(0,"12") #default value
    
    # Create generated pass field
    genpass = tk.StringVar()
    genpass.set("Generated Password")
    genlbl = tk.Entry(root,textvariable=genpass,fg="black",bg="white",state="readonly",width=50)
    genlbl.grid(column=1,row=7)
    # Create button
    def clicked():
        password = passent.get()
        domain = domainent.get()
        length = lenent.get()
        password = gen(password,domain,length)
        genpass.set(password)
    spacer2 = tk.Label(root,text="",bg=defaultcolor)
    spacer2.grid(column=0,row=6)
    btn = tk.Button(root, text="Generate",command=clicked)
    btn.grid(column=1, row=5)


    root.mainloop()
